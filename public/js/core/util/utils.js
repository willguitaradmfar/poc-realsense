inject.define("core.util.utils", [
		"corePatternClient.angular.service",
		"corePatternClient.config.config",
        function(service, _config) {
        	service.addService('Quadrante', ['RealTimeService', function(RealTimeService){
                var self = {};                   
                
                self.Quadrante = function(config){
                	var matriz = {};

                	config = config || {};

                	var maxX = 606;
                    var minX = 0;
                    var maxY = 456;
                    var minY = 0;

                    var quadranteX = maxX / config.qtdeQuadranteX || 1;
                    var quadranteY = maxY / config.qtdeQuadranteY || 1;

                    for(var x = 0 ; x < config.qtdeQuadranteX ; x++){                    	
                    	for(var y = 0 ; y < config.qtdeQuadranteY ; y++){                    		
                    		matriz[x+'_'+y] = {
                    			minX : (x * quadranteX),
                    			minY : (y * quadranteY),
                    			maxX : (x * quadranteX)+quadranteX,
                    			maxY : (y * quadranteY)+quadranteY
                    		};
                    	}
                    }

                    var makePoly = function(){
                    	var poly = {};
	                    for(var y = 0 ; y < config.qtdeQuadranteY ; y++){                    		
	                    	for(var x = 0 ; x < config.qtdeQuadranteX ; x++){
	                    		
	                    		poly[x+'_'+y] = poly[x+'_'+y] || [];

	                    		poly[x+'_'+y].push({
	                    			x : matriz[x+'_'+y].minX,
	                    			y : matriz[x+'_'+y].minY
	                    		});

	                    		poly[x+'_'+y].push({
	                    			x : matriz[x+'_'+y].maxX,
	                    			y : matriz[x+'_'+y].minY
	                    		});

	                    		poly[x+'_'+y].push({
	                    			x : matriz[x+'_'+y].maxX,
	                    			y : matriz[x+'_'+y].maxY
	                    		});

	                    		poly[x+'_'+y].push({
	                    			x : matriz[x+'_'+y].minX,
	                    			y : matriz[x+'_'+y].maxY
	                    		});

	                    		poly[x+'_'+y].push({
	                    			x : matriz[x+'_'+y].minX,
	                    			y : matriz[x+'_'+y].minY
	                    		});
	                    		
	                    	}	
	                    }	

	                    return poly;
                    };
                    
                    var polys = makePoly();

	                var isPointInPoly = function(poly, pt){
						for(var c = false, i = -1, l = poly.length, j = l - 1; ++i < l; j = i)
							((poly[i].y <= pt.y && pt.y < poly[j].y) || (poly[j].y <= pt.y && pt.y < poly[i].y))
							&& (pt.x < (poly[j].x - poly[i].x) * (pt.y - poly[i].y) / (poly[j].y - poly[i].y) + poly[i].x)
							&& (c = !c);
						return c;
					};                    

                    RealTimeService.setListen('realsense::coordenadas', function(data){
                    	var x = parseInt(data.split(',')[0]);
                        var y = parseInt(data.split(',')[1]);

                        for(var i in polys){
                        	if(isPointInPoly(polys[i], {x : x, y : y})){                        		
                        		if(config.fns){
                        			config.fns(i, x, y);
                        		}
                        	}                        	
                        }
                    });

                };

                return self;

            }]);

			return {};
            
        }
]);