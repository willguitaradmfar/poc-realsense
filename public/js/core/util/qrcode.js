inject.define("core.util.qrcode", [
    "corePatternClient.angular.directive",
        function(directive) {
        	directive.addDirective('qr', [function(){
                return {
                            restrict: 'A',
                            scope: {
                                codeproduct : '='
                            },                            
                            replace : false,
                            link: function(scope, element, attr) {

                                $(element).html5_qrcode(function(data){
                                        console.log(data);                                        
                                        if(scope.codeproduct)
                                            scope.codeproduct(data);
                                    },
                                    function(error){
                                        if(error.message == "Cannot read property 'getContext' of null"){
                                            $(element).html5_qrcode_stop();
                                        }
                                        //console.error(error);
                                    }, function(videoError){
                                        console.error(videoErro);
                                    }
                                );
                            }
                        };
            }]);

			return {};
            
        }
]);