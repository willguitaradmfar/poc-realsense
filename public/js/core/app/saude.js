inject.define("core.app.saude", [
        "corePatternClient.angular.angular",
        "corePatternClient.angular.view",
        "corePatternClient.angular.angular",
        "corePatternClient.angular.controller", 
        "corePatternClient.service.socketio",
        "corePatternClient.service.realtime",
        "corePatternClient.angular.directive",
        "corePatternClient.angular.service",
        "corePatternClient.service.restful",
        "core.util.utils",        
        "core.util.qrcode",
        "core.app.saude-topo",
        "core.app.saude-service",
        "core.app.saude-cadastrar",
        "core.app.saude-home",
        "core.app.saude-login",
        "core.app.saude-pront",
        "core.app.saude-aplicar-medicamento",
        "core.app.saude-lista",
        "core.app.saude-exame",
        "core.app.saude-pront-modal",
        "core.app.saude-listaexames",
        function(angular) {
                var self = {};
                angular.start();
                return self;
        }
]);