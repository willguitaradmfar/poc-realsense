inject.define("core.app.agro-detalhe", [
        "corePatternClient.angular.view",        
        "core.config.config",        
        function(view, config) {
                var self = {};
                
                view.addView('detalhe', ['$scope', 'RealTimeService', 'QuadranteService', 'DBService', 'SocketIOService', 'UserService', 'OSService', 'OSDetalheTarefaService', function($scope, RealTimeService, QuadranteService, DBService, SocketIOService, UserService, OSService, OSDetalheTarefaService){
                    

                    var timeOutClick;
                    var select;
                    var cacheQuadrante;

                    $scope.os = OSService.os;

                    OSService.os.detalhe.map(function (o) {
                        
                        if(!o.percorridoP)o.percorridoP = 0;

                        if(!o.dtini)return;

                        if(o.concluido) return;

                        if(o.descricao) return;

                        var nowTime = new Date().getTime();
                        var objTime = o.dtini.getTime();

                        o.percorrido = (nowTime - objTime) / 1000 / 60;

                        var p = (o.percorrido*100)/o.previsao;

                        o.percorridoP = (p >= 100 ? 100 : p);
                    });

                    RealTimeService.removeListen('realsense::faceRecognition');
                    
                    $scope.options = [{
                        nome : 'Voltar',
                        url : '#/listados'
                    }];                    

                    $scope.user = UserService.user;

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0' || data == 'hand=many'){
                                cacheQuadrante = undefined;
                                for(var i in $scope.options){
                                    $scope.options[i]._class = '';
                                    select = undefined;                                    
                                }
                                for(var i in $scope.os.detalhe){
                                    $scope.os.detalhe[i]._class = '';
                                }
                                $scope.$digest();
                            } if(data == 'hand=click'){
                                click();
                            }
                            console.log(data);
                        });    
                    }, config.timeout);

                   var click = function(){                        
                        console.log(select);
                        if(select && select.url){
                           location.href = select.url;
                           return;
                        }

                        if(select && !select.concluida){
                            OSDetalheTarefaService.detalheTarefa = select;
                            if(select.iniciado){
                                location.href = '#/detalheTarefaIniciada';
                            }else{
                                location.href = '#/detalheTarefa';
                            }
                            return;
                        }

                        $scope.selected = select;                        
                        $scope.$digest();
                   };

                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                          click();
                        },config.temporizador);
                   };

                    setTimeout(function () {
                        new QuadranteService.Quadrante({
                            qtdeQuadranteX : 1,
                            qtdeQuadranteY : ($scope.os.detalhe.length + 1),
                            fns : function(index){
                                
                                 if(cacheQuadrante == index)return;
                                cacheQuadrante = index;

                                osIndex = index.split('_')[1];
                                index = index.split('_')[0];
                                
                                index = (parseInt(index) * -1) + ($scope.options.length - 1);
                                
                                for(var i in $scope.options){
                                    $scope.options[i]._class = undefined;
                                }
                                
                                for(var i in $scope.os.detalhe){
                                    $scope.os.detalhe[i]._class = undefined;
                                }

                                if(osIndex == 0 && index == 0){
                                    $scope.options[index]._class = 'hvr-sweep-to-top';
                                    select = $scope.options[index];
                                }
                                
                                if(osIndex > 0){
                                    $scope.os.detalhe[(osIndex-1)]._class = 'hvr-sweep-to-top';
                                    select = $scope.os.detalhe[(osIndex-1)];
                                }
                                
                                $scope.$digest();
                                changeOption();                            
                            }
                        });
                    }, config.timeout);
                }]);

                return self;
        }
]);