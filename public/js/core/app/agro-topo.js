inject.define("core.app.agro-topo", [
        "corePatternClient.angular.view",
        "corePatternClient.angular.angular",
        "corePatternClient.angular.controller", 
        "corePatternClient.service.socketio",
        "corePatternClient.service.realtime",
        "corePatternClient.angular.directive",
        "corePatternClient.angular.service",
        "corePatternClient.service.restful",
        "core.util.utils",
        "core.config.config",
        "core.util.qrcode",
        function(view, angular, controller, socketio, realtime, directive, service, restful, utilll, config) {
                var self = {};

                controller.addController('Topo', ['$scope', 'RealTimeService', 'UserService', function($scope, RealTimeService, UserService){

                    /*RealTimeService.setListen('realsense::takePhoto', function(data){
                        $scope.image = data;
                        $scope.$digest();
                    });*/

                    RealTimeService.setListen('realsense::userautenticado', function(data){
                        $scope.user = data;
                        $scope.$digest();
                    });

                }]);

                return self;
        }
]);