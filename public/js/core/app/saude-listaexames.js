inject.define("core.app.saude-listaexames", [
        "corePatternClient.angular.view",        
        "core.config.config",        
        function(view, config) {
                var self = {};                

                view.addView('listaexames', ['$scope', 'RealTimeService', 'QuadranteService', 'SaudeCollectionService', 'UserService', 'PageService', 
                    function($scope, RealTimeService, QuadranteService, SaudeCollectionService, UserService, PageService){
                    

                     $scope.item = 0;

                     var cacheQuadrante;

                     $scope.options = [{
                        nome : 'Voltar',
                        url : '#/pront'
                      },{
                        nome : 'Prev',
                        type : 'action'
                      },{
                        nome : 'Next',
                        type : 'action'
                      }];
                    
                    if(!SaudeCollectionService.saude){
                        location.href = '#/login';
                        return;
                    }

                    var imparPar = (UserService.user.ID % 2 == 0 ? 'par' : 'impar');

                    $scope.paciente = SaudeCollectionService.saude[imparPar].pacientes['10'];
                    
                    $scope.paciente.dtEntrada = new Date($scope.paciente.dtEntrada || new Date().getTime());

                    RealTimeService.removeListen('realsense::faceRecognition');

                    $scope.paciente.horariosExames.map(function (o, i) {                                          
                        o.horario = moment()
                                        .subtract((2*i), 'days')                                        
                                        .toDate();

                        o.exames.map(function (_o, _i) {
                            _o.horario = moment()
                                        .subtract((2*i), 'days')
                                        .add((Math.random() * 10), 'hours')
                                        .toDate();
                        });
                    });

                   var select;

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){                           
                            if(data == 'hand=0' || data == 'hand=many'){
                                cacheQuadrante = undefined;
                                for(var i in $scope.options){
                                    $scope.options[i]._class = '';                                    
                                    $scope.$digest();
                                }

                                for(var i in $scope.paciente.horariosExames){
                                    var h = $scope.paciente.horariosExames[i];
                                    for(var ii in h.exames){
                                        var med = h.exames[ii];                                        
                                        med._class = '';
                                        $scope.$digest();
                                    }
                                }

                                select = undefined;

                            } if(data == 'hand=click'){
                                click();
                            }
                            console.log(data);
                        });    
                    }, config.timeout);

                var click = function(){

                    if(!select)return;

                    if(select.url){
                        location.href = select.url;
                        return;
                    }

                    if(select.type == 'action' && select.nome == 'Prev' && $scope.item > 0){
                        $scope.item--;                        
                        cacheQuadrante = '';
                    }else if(select.type == 'action' && select.nome == 'Next' && $scope.item < ($scope.paciente.horariosExames.length - 1)){
                        $scope.item++;                        
                        cacheQuadrante = '';
                    }
                    
                    
                    $scope.selected = select;
                    $scope.$digest();
                };

                   var timeOutClick;
                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                           click();
                        },config.temporizador);
                   };

                    setTimeout(function () {
                        new QuadranteService.Quadrante({
                            qtdeQuadranteX : 3,
                            qtdeQuadranteY : 3,
                            fns : function(index){

                                if(cacheQuadrante == index)return;
                                cacheQuadrante = index;                                

                                var y = index.split('_')[1];
                                var yy = index.split('_')[1];
                                y = (parseInt(y) * -1) + (3 - 1);

                                var x = index.split('_')[0];
                                x = (parseInt(x) * -1) + (3 - 1);

                                for(var i in $scope.options){
                                    $scope.options[i]._class = undefined;
                                }

                                for(var i in $scope.paciente.horariosExames){
                                    var h = $scope.paciente.horariosExames[i];
                                    for(var ii in h.exames){
                                        var med = h.exames[ii];
                                        med._class = undefined;
                                    }
                                }

                                if(x == 0 && y == 2){
                                    $scope.options[0]._class = 'hvr-sweep-to-top';
                                    select = $scope.options[0];
                                }else if(x == 0 && (y == 1 || y == 0)){
                                    $scope.options[1]._class = 'hvr-sweep-to-top';                                    
                                    select = $scope.options[1];
                                }else if(x == 2 && (y == 1 || y == 0)){
                                    $scope.options[2]._class = 'hvr-sweep-to-top';
                                    select = $scope.options[2];                                    
                                }else if(x == 1){
                                    if($scope.paciente.horariosExames[$scope.item].exames[yy]){
                                        select = {
                                            medicamento : $scope.paciente.horariosExames[$scope.item].exames[yy],
                                            horario : $scope.paciente.horariosExames[$scope.item]
                                        }
                                        $scope.paciente.horariosExames[$scope.item].exames[yy]._class = 'hvr-sweep-to-top';                                        
                                    }
                                }
                                
                                $scope.$digest();
                                changeOption(); 
                            }
                        });
                    }, config.timeout);
                }], ['/dependency/moment/moment.js']);

                return self;
        }
]);