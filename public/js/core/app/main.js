inject.define("core.app.main", [
        "corePatternClient.angular.view",
        "corePatternClient.angular.angular",
        "corePatternClient.service.socketio",
        "corePatternClient.service.realtime",
        "corePatternClient.angular.directive",
        "corePatternClient.angular.service",
        "corePatternClient.service.restful",
        "core.util.utils",
        "core.config.config",
        "core.util.qrcode",
        function(view, angular, socketio, realtime, directive, service, restful, utilll, config) {
                var self = {};

                service.addService('DB', function($resource){
                        return $resource('/:opr/:key/:value', [], [], {});
                });

                // view.addView('demo', ['$scope', 'SocketIOService', 'RealTimeService', 'DBService', function($scope, SocketIOService, RealTimeService, DBService){
                //         var select;

                //         setTimeout(function(){
                //             RealTimeService.setListen('realsense::selecionar', function(data){
                //                 console.log(data); 
                //                 if(select == 'voltar'){
                //                     location.href = "#/telaInicial";
                //                 }else if(select == 'proximo'){
                //                      location.href = "#/lista";
                //                 }                                
                //             });    
                //         }, 1000);

                //         var maxX = 606;
                //         var minX = 0;
                //         var maxY = 456;
                //         var minY = 0;

                //         var quadranteX = maxX / 2;
                //         var quadranteY = maxX / 2;

                //         RealTimeService.setListen('realsense::coordenadas', function(data){
                //                 var x = parseInt(data.split(',')[0]);
                //                 var y = parseInt(data.split(',')[1]);

                //                 console.log(maxX, maxY, minX, minY);

                //                  if(x > quadranteX && y < quadranteY){
                //                     select = 'voltar';
                //                     $scope.hovervoltar = 'warning';
                //                     $scope.hoverproximo = 'success';                                    
                //                     $scope.$digest()
                //                     console.log(1);
                //                 }else if(x < quadranteX && y < quadranteY){
                //                     select = 'proximo';
                //                     $scope.hovervoltar = 'success';
                //                     $scope.hoverproximo = 'warning';                                    
                //                     $scope.$digest()
                //                     console.log(2);
                //                 }
                //         });

                //         RealTimeService.setListen('realsense::faceRecognition', function(data){
                //                 $scope.ID = encodeURI(data).toString().replace(/%00/g, '').replace(/%20/g, ' ');
                                
                //                 if($scope.ID > 0){
                //                         if(!$scope.user)$scope.user = {};

                //                         DBService.get({opr : 'get', key : 'UserName'+$scope.ID}, function(data){
                //                                 $scope.text = (data.value ? 'Usuário '+ data.value : 'Você não está cadastrado');
                //                         });
                //                         DBService.get({opr : 'get', key : 'UserFuncao'+$scope.ID}, function(data){
                //                                 $scope.user.funcao = data.value;
                //                         });
                //                 }else{
                //                        $scope.text = 'Aguarde, Calculando biometria .....';
                //                 }
                //                 $scope.$digest();
                //         });    

                //          RealTimeService.setListen('realsense::speechRecognition', function(data){

                //                 var speech = encodeURI(data).toString().replace(/%00/g, '').replace(/%20/g, ' ');
                //                 console.log(speech);
                //                 if(speech.toLowerCase() == "Voltar".toLowerCase()
                //                         || speech.toLowerCase() == "Home".toLowerCase()){
                //                         location.href = "#/telaInicial"
                //                 }else if(speech.toLowerCase() == "Ok".toLowerCase()){
                //                         $scope.ok();
                //                 }else{
                //                          // $scope.user = {};
                //                          // $scope.user.nome = speech;
                //                          // $scope.$digest();

                //                          $scope.speech = speech;
                //                          $scope.$digest();
                //                 }
                //         });
                        
                // }]);

                view.addView('demo', ['$scope', 'RealTimeService', 'QuadranteService', 'DBService', function($scope, RealTimeService, QuadranteService, DBService){
                    
                    $scope.qrproduct = function(p){
                        alert(p);
                        DBService.get({opr : 'get', key : 'Product'+p}, function(data){
                            data = JSON.parse(data.value);
                            $scope.product = data;
                        }); 
                    };
                    

                    RealTimeService.setListen('realsense::faceRecognition', function(data){
                            $scope.ID = encodeURI(data).toString().replace(/%00/g, '').replace(/%20/g, ' ');
                            
                            if($scope.ID > 0){
                                    if(!$scope.user)$scope.user = {};

                                    DBService.get({opr : 'get', key : 'User'+$scope.ID}, function(data){
                                        data = JSON.parse(data.value);
                                        $scope.text = (data ? 'Usuário '+ data.nome : 'Você não está cadastrado');
                                    });                                    
                            }else{
                                   $scope.text = 'Aguarde, Calculando biometria .....';
                            }
                            $scope.$digest();
                    }); 

                    var timeOutClick;

                      $scope.meds = [{
                        nome : 'Voltar'
                      },{
                        nome : 'Proximo'
                      }];

                   var select;

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0'){
                                for(var i in $scope.meds){
                                    $scope.meds[i]._class = '';
                                    select = undefined;
                                    $scope.$digest();
                                }
                            } if(data == 'hand=click'){
                                click();
                            }
                            console.log(data);
                        });    
                    }, 1000);

                   var click = function(){
                        if(select && select.nome == 'Voltar'){
                           location.href = "#/telaInicial";                               
                        }

                        if(select && select.nome == 'Proximo'){
                           location.href = "#/lista";                               
                        }
                        $scope.selected = select;
                        $scope.$digest();
                   };

                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                           click();
                        },config.temporizador);
                   };
                    
                    new QuadranteService.Quadrante({
                        qtdeQuadranteX : $scope.meds.length,
                        qtdeQuadranteY : 2,
                        fns : function(index){
                            index = index.split('_')[0];
                            index = (parseInt(index) * -1) + ($scope.meds.length - 1);
                            
                            if(select && $scope.meds[index].$$hashKey == select.$$hashKey){
                                return;
                            }
                            for(var i in $scope.meds){
                                $scope.meds[i]._class = undefined;
                            }
                            $scope.meds[index]._class = 'warning';
                            select = $scope.meds[index];
                            $scope.$digest();
                            changeOption();                            
                        }
                        });
                }]);

                // view.addView('telaIniciala', ['$scope', 'SocketIOService', 'RealTimeService', 'RestfulQueryService', function($scope, SocketIOService, RealTimeService, RestfulQueryService){
                //         var select;                        
                //         setTimeout(function(){
                //             RealTimeService.setListen('realsense::selecionar', function(data){
                //                 console.log(data);
                //                 location.href = "#/"+select;
                                
                //             });
                //         }, 1000);

                //         var maxX = 606;
                //         var minX = 0;
                //         var maxY = 456;
                //         var minY = 0;

                //         var quadranteX = maxX / 2;
                //         var quadranteY = maxX / 2;

                //         RealTimeService.setListen('realsense::coordenadas', function(data){
                //                 var x = parseInt(data.split(',')[0]);
                //                 var y = parseInt(data.split(',')[1]);                               

                //                 console.log(maxX, maxY, minX, minY);

                //                  if(x > quadranteX && y < quadranteY){
                //                     select = 'cadastrar';
                //                     $scope.hovercadastrar = 'warning';
                //                     $scope.hoverdemo = 'success';                                    
                //                     $scope.$digest()
                //                     console.log(1);
                //                 }else if(x < quadranteX && y < quadranteY){
                //                     select = 'demo';
                //                     $scope.hovercadastrar = 'success';
                //                     $scope.hoverdemo = 'warning';                                    
                //                     $scope.$digest()
                //                     console.log(2);
                //                 }
                //         });

                //         RealTimeService.setListen('realsense::speechRecognition', function(data){
                //                 var speech = encodeURI(data).toString().replace(/%00/g, '').replace(/%20/g, ' ');
                //                 console.log(speech);                                
                //                 if(speech.toLowerCase() == "Cadastrar".toLowerCase()
                //                         || speech.toLowerCase() == "Cadastro".toLowerCase()){
                //                         location.href = "#/cadastrar"
                //                 }

                //                 if(speech.toLowerCase() == "Demonstrar".toLowerCase()
                //                         || speech.toLowerCase() == "Demo".toLowerCase()){
                //                         location.href = "#/demo"
                //                 }

                //         });
                        
                // }]);

                view.addView('telaInicial', ['$scope', 'RealTimeService', 'QuadranteService', function($scope, RealTimeService, QuadranteService){                    


                    var timeOutClick;

                      $scope.meds = [{
                        nome : 'Cadastro'
                      },{
                        nome : 'Demonstração'
                      }];

                   var select;

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0'){
                                for(var i in $scope.meds){
                                    $scope.meds[i]._class = '';
                                    select = undefined;
                                    $scope.$digest();
                                }
                            } if(data == 'hand=click'){
                                click();
                            }
                            console.log(data);
                        });    
                    }, 1000);


                   var click = function(){
                        if(select && select.nome == 'Cadastro'){
                           location.href = "#/cadastrar";                               
                        }

                        if(select && select.nome == 'Demonstração'){
                           location.href = "#/demo";                               
                        }
                        $scope.selected = select;
                        $scope.$digest();
                   };

                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){

                           click();
                        },config.temporizador);
                   };
                    
                    new QuadranteService.Quadrante({
                        qtdeQuadranteX : $scope.meds.length,
                        qtdeQuadranteY : 2,
                        fns : function(index){
                            index = index.split('_')[0];
                            index = (parseInt(index) * -1) + ($scope.meds.length - 1);
                            
                            if(select && $scope.meds[index].$$hashKey == select.$$hashKey){
                                return;
                            }
                            for(var i in $scope.meds){
                                $scope.meds[i]._class = undefined;
                            }
                            $scope.meds[index]._class = 'warning';
                            select = $scope.meds[index];
                            $scope.$digest();
                            changeOption();                            
                        }
                    });
                }]);

                view.addView('lista', ['$scope', 'RealTimeService', 'QuadranteService', function($scope, RealTimeService, QuadranteService){
                    
                    var timeOutClick;

                  $scope.meds = [{
                    nome : 'Voltar',
                    posologia : ' ...',
                    img : '/img/voltar.jpg'
                  },{
                    nome : 'Dipirona',
                    posologia : '10 gt a cada 6h',
                    img : '/img/dipirona.jpg'
                  } , {
                    nome : 'Atenolol',
                    posologia : '1 cp a cada 8h',
                    img : '/img/atenolol.jpg'
                  }, {
                    nome : 'Propranolol',
                    posologia : '1 cp a cada 8h'                        ,
                    img : '/img/propranolol.jpg'
                  }
                  // , {
                  //   nome : 'Glicose',
                  //   posologia : '....',
                  //   img : '/img/glicose.jpg'
                  // }, {
                  //   nome : 'Glibenclamida',
                  //   posologia : '....',
                  //   img : '/img/glibenclamida.jpg'
                  // }, {
                  //   nome : 'Paracetamol',
                  //   posologia : '....',
                  //   img : '/img/paracetamol.jpg'
                  // }, {
                  //   nome : 'Teste',
                  //   posologia : '....',
                  //   img : '/img/default.jpg'
                  // }
                  ];


                   var select;

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0'){
                                for(var i in $scope.meds){
                                    $scope.meds[i]._class = '';
                                    select = undefined;
                                    $scope.$digest();
                                }
                            } if(data == 'hand=click'){                                
                                click();
                            }
                            console.log(data);
                        });    
                    }, 1000);

                   var scrol = function(index){
                        window.scrollTo(0, index * 100);
                   };

                   var click = function(){
                        if(select && select.nome == 'Voltar'){
                           location.href = "#/demo";                               
                        }
                        $scope.selected = select;
                        $scope.$digest();
                   }

                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                            click();
                        },config.temporizador);
                   };
                    
                    new QuadranteService.Quadrante({
                        qtdeQuadranteX : 1,
                        qtdeQuadranteY : $scope.meds.length,
                        fns : function(index){
                            index = index.split('_')[1];
                            if(select && $scope.meds[index].$$hashKey == select.$$hashKey){
                                return;
                            }
                            for(var i in $scope.meds){
                                $scope.meds[i]._class = '';
                            }
                            $scope.meds[index]._class = 'active';
                            select = $scope.meds[index];
                            $scope.$digest();
                            changeOption();
                            scrol(index);
                        }
                    });
                }]);

                // view.addView('cadastrar', ['$scope', 'SocketIOService', 'RealTimeService', 'DBService', function($scope, SocketIOService, RealTimeService, DBService){

                //         SocketIOService.emit('publish', {
                //                 channel : 'realsense::speechSynthesis',
                //                 data : 'Preencha o formulário'
                //         });

                //         var select;

                //         setTimeout(function(){
                //             RealTimeService.setListen('realsense::selecionar', function(data){
                                
                //                 if(select == 'voltar'){
                //                     location.href = "#/telaInicial";
                //                 }else if(select == 'confimar'){
                //                      $scope.ok();
                //                 }
                                
                //             });    
                //         }, 1000);                        

                //         var maxX = 606;
                //         var minX = 0;
                //         var maxY = 456;
                //         var minY = 0;

                //         var quadranteX = maxX / 2;
                //         var quadranteY = maxX / 2;

                //         RealTimeService.setListen('realsense::coordenadas', function(data){
                //                 var x = parseInt(data.split(',')[0]);
                //                 var y = parseInt(data.split(',')[1]);                               

                //                 console.log(maxX, maxY, minX, minY);

                //                  if(x > quadranteX && y < quadranteY){
                //                     select = 'voltar';
                //                     $scope.hovervoltar = 'warning';
                //                     $scope.hoverconfimar = 'success';                                    
                //                     $scope.$digest()
                //                     console.log(1);
                //                 }else if(x < quadranteX && y < quadranteY){
                //                     select = 'confimar';
                //                     $scope.hovervoltar = 'success';
                //                     $scope.hoverconfimar = 'warning';                                    
                //                     $scope.$digest()
                //                     console.log(2);
                //                 }
                //         });

                //         RealTimeService.setListen('realsense::speechRecognition', function(data){

                //                 var speech = encodeURI(data).toString().replace(/%00/g, '').replace(/%20/g, ' ');
                //                 console.log(speech);
                //                 if(speech.toLowerCase() == "Voltar".toLowerCase()
                //                         || speech.toLowerCase() == "Home".toLowerCase()){
                //                         location.href = "#/telaInicial"
                //                 }else if(speech.toLowerCase() == "Ok".toLowerCase()
                //                         || speech.toLowerCase() == "Confirmar".toLowerCase()
                //                         || speech.toLowerCase() == "Gravar".toLowerCase()){
                //                         $scope.ok();
                //                 }else{
                //                          $scope.speech = speech;
                //                          $scope.$digest();
                //                 }
                //         });

                //         var cacheFaceRecognition;

                //         RealTimeService.setListen('realsense::faceRecognition', function(data){
                //                 $scope.ID = encodeURI(data).toString().replace(/%00/g, '').replace(/%20/g, ' ');
                //                 $scope.textWarn = ($scope.ID < 0 ? 'Aguarde, Calculando biometria .....' : $scope.ID);
                //                 if($scope.ID > 0 && cacheFaceRecognition != $scope.ID){
                //                         if(!$scope.user)$scope.user = {};

                //                         cacheFaceRecognition = $scope.ID;

                //                         DBService.get({opr : 'get', key : 'UserName'+$scope.ID}, function(data){
                //                                 $scope.user.nome = data.value;
                //                         });
                //                         DBService.get({opr : 'get', key : 'UserFuncao'+$scope.ID}, function(data){
                //                                 $scope.user.funcao = data.value;
                //                         });
                //                 }
                //                 $scope.$digest();
                //         });

                //         var cache;

                //         RealTimeService.setListen('realsense::gestureRecognition', function(data){

                //                 if(cache)return;                                
                //                 cache = true;

                //                 setTimeout(function(){
                //                         cache = undefined;
                //                 }, 1000 * 5);

                //                 var speech = encodeURI(data).toString().replace(/%00/g, '').replace(/%20/g, ' ');
                //                 console.log(speech);
                //                 if(speech.toLowerCase() == "thumb_up".toLowerCase()){
                //                         $scope.ok();
                //                 }
                //         });

                //         $scope.ok = function(){

                //                 DBService.get({opr : 'set', key : 'UserName'+$scope.ID, value : $scope.user.nome});

                //                 DBService.get({opr : 'set', key : 'UserFuncao'+$scope.ID, value : $scope.user.funcao});

                //                 SocketIOService.emit('publish', {
                //                         channel : 'realsense::speechSynthesis',
                //                         data : ['O', $scope.user.funcao, ',', $scope.user.nome, ', foi', 'Cadastrado'].join(' ')
                //                 });

                //                 $scope.text = "Cadastrado com sucesso";
                //         };
                // }]);

                view.addView('cadastrar', ['$scope', 'RealTimeService', 'QuadranteService', 'DBService', 'SocketIOService', function($scope, RealTimeService, QuadranteService, DBService, SocketIOService){


                    $scope.ok = function(){

                            DBService.get({opr : 'set', key : 'User'+$scope.ID, value : JSON.stringify($scope.user)});

                            SocketIOService.emit('publish', {
                                    channel : 'realsense::speechSynthesis',
                                    data : ['O', $scope.user.funcao, ',', $scope.user.nome, ', foi', 'Cadastrado'].join(' ')
                            });

                            $scope.text = "Cadastrado com sucesso";
                    };

                    var cacheFaceRecognition;
                    RealTimeService.setListen('realsense::faceRecognition', function(data){
                            $scope.ID = encodeURI(data).toString().replace(/%00/g, '').replace(/%20/g, ' ');
                            $scope.textWarn = ($scope.ID < 0 ? 'Aguarde, Calculando biometria .....' : $scope.ID);
                            if($scope.ID > 0 && cacheFaceRecognition != $scope.ID){
                                    if(!$scope.user)$scope.user = {};

                                    cacheFaceRecognition = $scope.ID;

                                    DBService.get({opr : 'get', key : 'User'+$scope.ID}, function(data){
                                            $scope.user= JSON.parse(data.value);
                                    });                                    
                            }
                            $scope.$digest();
                    });

                    var timeOutClick;

                      $scope.meds = [{
                        nome : 'Voltar'
                      },{
                        nome : 'Confirmar'
                      }];

                   var select;

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0'){
                                for(var i in $scope.meds){
                                    $scope.meds[i]._class = '';
                                    select = undefined;
                                    $scope.$digest();
                                }
                            } if(data == 'hand=click'){
                                click();
                            }
                            console.log(data);
                        });    
                    }, 1000);

                   var click = function(){
                        if(select && select.nome == 'Voltar'){
                           location.href = "#/telaInicial";                               
                        }

                        if(select && select.nome == 'Confirmar'){
                           $scope.ok();                               
                        }
                        $scope.selected = select;
                        $scope.$digest();
                   };

                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                          click();
                        },config.temporizador);
                   };
                    
                    new QuadranteService.Quadrante({
                        qtdeQuadranteX : $scope.meds.length,
                        qtdeQuadranteY : 2,
                        fns : function(index){
                            index = index.split('_')[0];
                            index = (parseInt(index) * -1) + ($scope.meds.length - 1);
                            
                            if(select && $scope.meds[index].$$hashKey == select.$$hashKey){
                                return;
                            }
                            for(var i in $scope.meds){
                                $scope.meds[i]._class = undefined;
                            }
                            $scope.meds[index]._class = 'warning';
                            select = $scope.meds[index];
                            $scope.$digest();
                            changeOption();                            
                        }
                    });
                }]);
                
                angular.start();

                return self;
        }
]);