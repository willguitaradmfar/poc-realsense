inject.define("core.app.saude-exame", [
        "corePatternClient.angular.view",        
        "core.config.config",        
        function(view, config) {
                var self = {};              
               
                view.addView('exame', ['$scope', 'RealTimeService', 'QuadranteService', function($scope, RealTimeService, QuadranteService){
                    
                    $scope.options = [{
                        nome : 'Voltar',
                        url : '#/pront'
                      }];

                    RealTimeService.removeListen('realsense::faceRecognition');

                   var select;

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0' || data == 'hand=many'){
                                cacheQuadrante = undefined;
                                for(var i in $scope.options){
                                    $scope.options[i]._class = '';
                                    select = undefined;
                                    $scope.$digest();
                                }
                            } if(data == 'hand=click'){
                                click();
                            }
                            console.log(data);
                        });    
                    }, config.timeout);


                   var click = function(){
                        if(select && !select.url)return;
                        location.href = select.url;
                        
                        $scope.selected = select;
                        $scope.$digest();
                   };

                   var timeOutClick;
                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                           click();
                        },config.temporizador);
                   };

                    var cacheQuadrante;

                    setTimeout(function () {
                        new QuadranteService.Quadrante({
                            qtdeQuadranteX : $scope.options.length,
                            qtdeQuadranteY : 1,
                            fns : function(index){

                                if(cacheQuadrante == index)return;
                                cacheQuadrante = index;

                               index = index.split('_')[0];
                                index = (parseInt(index) * -1) + ($scope.options.length - 1);                            
                               
                                for(var i in $scope.options){
                                    $scope.options[i]._class = undefined;
                                }
                                
                                $scope.options[index]._class = 'hvr-sweep-to-top';
                                select = $scope.options[index];
                                $scope.$digest();
                                changeOption();                            
                            }
                        });
                    }, config.timeout);
                }]);

                return self;
        }
]);