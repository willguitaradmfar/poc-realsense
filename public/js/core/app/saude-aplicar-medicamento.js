inject.define("core.app.saude-aplicar-medicamento", [
        "corePatternClient.angular.view",        
        "core.config.config",        
        function(view, config) {
                var self = {};              
               
                view.addView('aplicarMedicamento', ['$scope', 'RealTimeService', 'QuadranteService', 'SaudeCollectionService', 'UserService', 
                    function($scope, RealTimeService, QuadranteService, SaudeCollectionService, UserService){

                    $scope.options = [{
                        nome : 'Voltar',
                        url : '#/lista'
                      }];                      

                    if(!SaudeCollectionService.saude){
                        location.href = '#/login';
                        return;
                    }

                    var imparPar = (UserService.user.ID % 2 == 0 ? 'par' : 'impar');

                    $scope.paciente = SaudeCollectionService.saude[imparPar].pacientes['10'];
                    
                    var collection = SaudeCollectionService.saude[UserService.user.ID];

                    $scope.medicamento = SaudeCollectionService.medicamento;
                    $scope.horario = SaudeCollectionService.horario;

                    if(!$scope.medicamento.fase){
                        $scope.medicamento.fase = 0;
                        setTimeout(function () {

                            $scope.medicamento.fase = 1;
                            $scope.$digest();

                            setTimeout(function () {

                                $scope.medicamento.fase = 2;
                                $scope.$digest();

                                setTimeout(function () {                                    

                                    var d10plus =  moment().add(10, 'minute').toDate().getTime();
                                    var d10subtract =  moment().subtract(10, 'minute').toDate().getTime();
                                    var current = new Date($scope.horario.horario).getTime();

                                    if(d10subtract <= current && d10plus >= current){
                                        $scope.medicamento.fase = 3;
                                        $scope.$digest();
                                    }else{
                                        $scope.horarioIncorreto = true;
                                        $scope.$digest();
                                    }

                              }, config.transicaoCorretos);
                          }, config.transicaoCorretos);
                      }, config.transicaoCorretos);
                    }


                    setTimeout(function(){
                        RealTimeService.setListen('realsense::speechRecognition', function(data){                           
                            var palavras = data.split(' ');

                            palavras.map(function (o) {
                                if(o.toLowerCase() == 'confirmar' || o.toLowerCase() == 'confirmado' || o.toLowerCase() == 'confirmada'){
                                    if($scope.medicamento.fase == 4 && !$scope.successWait){
                                        $scope.medicamento.dosagemclass = 'done';
                                        $scope.medicamento.fase = 5;
                                        $scope.successWait = true;
                                        $scope.$digest();
                                        setTimeout(function () {
                                            $scope.successWait = false;
                                            $scope.$digest();
                                        }, config.transicaoCorretos);
                                    }

                                    if($scope.medicamento.fase == 5 && !$scope.successWait){                                
                                        $scope.medicamento.viaclass = 'done';
                                        $scope.medicamento.fase = 6;
                                        $scope.successWait = true;
                                        $scope.$digest();
                                        setTimeout(function () {
                                            $scope.successWait = false;
                                            $scope.$digest();
                                        }, config.transicaoCorretos);
                                    }
                                    $scope.$digest();        
                                }
                            });

                            
                        });    
                    }, config.timeout);
                      

                    $scope.qrproduct = function (data) {
                        if($scope.medicamento.fase != 3) return;

                        $scope.codigoMedicamento = data;

                        if($scope.medicamento.codigo == data){
                            $scope.medicamento.okclass = 'done';
                            $scope.medicamento.fase = 4;
                            $scope.successWait = true;
                            $scope.$digest();
                            setTimeout(function () {
                                $scope.successWait = false;
                                $scope.$digest();
                            }, config.transicaoCorretos);
                        }else{
                            
                            $scope.$digest();
                        }
                    };

                   RealTimeService.removeListen('realsense::faceRecognition');

                   var select;

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if($scope.medicamento.fase == 4 && data == 'hand=confirmacao' && !$scope.successWait){
                                $scope.medicamento.dosagemclass = 'done';
                                $scope.medicamento.fase = 5;
                                $scope.successWait = true;
                                $scope.$digest();
                                setTimeout(function () {
                                    $scope.successWait = false;
                                    $scope.$digest();
                                }, config.transicaoCorretos);
                            }

                            if($scope.medicamento.fase == 5 && data == 'hand=confirmacao' && !$scope.successWait){                                
                                $scope.medicamento.viaclass = 'done';
                                $scope.medicamento.fase = 6;
                                $scope.successWait = true;
                                $scope.$digest();
                                setTimeout(function () {
                                    $scope.successWait = false;
                                    $scope.$digest();
                                }, config.transicaoCorretos);
                            }

                            if(data == 'hand=0' || data == 'hand=many'){
                                cacheQuadrante = undefined;
                                for(var i in $scope.options){
                                    $scope.options[i]._class = '';
                                    select = undefined;
                                    $scope.$digest();
                                }
                            } if(data == 'hand=click'){
                                click();
                            }
                            console.log(data);
                        });    
                    }, config.timeout);


                   var click = function(){
                        if(select && !select.url)return;

                        location.href = select.url;
                        
                        $scope.selected = select;
                        $scope.$digest();
                   };

                   var timeOutClick;
                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                           click();
                        },config.temporizador);
                   };

                    var cacheQuadrante;

                    setTimeout(function () {
                        new QuadranteService.Quadrante({
                            qtdeQuadranteX : 3,
                            qtdeQuadranteY : 3,
                            fns : function(index){

                                if(cacheQuadrante == index)return;
                                cacheQuadrante = index;

                                var y = index.split('_')[1];
                                y = (parseInt(y) * -1) + (3 - 1);

                                var x = index.split('_')[0];
                                x = (parseInt(x) * -1) + (3 - 1);

                                for(var i in $scope.options){
                                    $scope.options[i]._class = undefined;
                                }

                                if(x == 0){
                                    $scope.options[0]._class = 'hvr-sweep-to-top';
                                    select = $scope.options[0];
                                }
                                
                                $scope.$digest();
                                changeOption(); 
                            }
                        });
                    }, config.timeout);
                }], ['/dependency/moment/moment.js']);

                return self;
        }
]);