inject.define("core.app.agro-login", [
        "corePatternClient.angular.view",
        "core.config.config",
        "corePatternClient.angular.directive",
        function(view, config, directive) {
                var self = {};
               
                view.addView('login', ['$scope', '$timeout', 'RealTimeService', 'QuadranteService', 'DBService', 'SocketIOService', 'UserService', 'OSCollectionService', 
                    function($scope, $timeout, RealTimeService, QuadranteService, DBService, SocketIOService, UserService, OSCollectionService){

                    var cacheFaceRecognition;
                    var cacheQuadrante;
                    var select;
                    var timeOutClick;

                    var timeOutNextView;

                    RealTimeService.setListen('realsense::faceRecognition', function(data){
                        $scope.ID = encodeURI(data).toString().replace(/%00/g, '').replace(/%20/g, ' ');                        

                        if($scope.ID > 0){

                            if(!$scope.user)$scope.user = {};

                            cacheFaceRecognition = $scope.ID;                            

                            DBService.get({opr : 'get', key : 'User'+$scope.ID}, function(data){

                                if(!data.value){
                                    $scope.alert = 'nao_cadastrado';
                                    return;
                                }                                

                                $scope.alert = 'cadastrado';
                                $scope.user= JSON.parse(data.value);

                                SocketIOService.emit('publish', {
                                    channel : 'realsense::userautenticado',
                                    data : $scope.user
                                });

                                if(UserService.user && UserService.user.nome != $scope.user.nome){
                                    OSCollectionService.reset();
                                }

                                UserService.user = $scope.user;
                                if(!timeOutNextView)
                                timeOutNextView = setTimeout(function () {
                                    location.href = '#/listados';
                                }, 1000 * 5);

                            });                                    
                        }else if($scope.ID < 0){
                            $scope.alert = 'calculando';
                        }

                        $scope.$digest();
                    });

                    $scope.options = [{
                        nome : 'Voltar',
                        url : '#/home'
                    }];

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0' || data == 'hand=many'){
                                cacheQuadrante = undefined;
                                for(var i in $scope.options){
                                    $scope.options[i]._class = '';
                                    select = undefined;
                                    $scope.$digest();
                                }
                            } if(data == 'hand=click'){
                                click();
                            }
                            console.log(data);
                        });    
                    }, config.timeout);

                    var click = function(){
                        if(select && select.url){
                            clearTimeout(timeOutNextView);
                            location.href = select.url;
                            return;
                        }

                        $scope.selected = select;                        
                        $scope.$digest();
                    };

                    var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                          click();
                        },config.temporizador);
                   };

                    setTimeout(function () {
                        new QuadranteService.Quadrante({
                            qtdeQuadranteX : $scope.options.length,
                            qtdeQuadranteY : 1,
                            fns : function(index){

                                if(cacheQuadrante == index)return;
                                cacheQuadrante = index;

                                index = index.split('_')[0];
                                index = (parseInt(index) * -1) + ($scope.options.length - 1);
                                for(var i in $scope.options){
                                    $scope.options[i]._class = undefined;
                                }
                                
                                $scope.options[index]._class = 'hvr-sweep-to-top';
                                select = $scope.options[index];
                                $scope.$digest();
                                changeOption();                            
                            }
                        });
                    }, config.timeout);
                }]);               

                return self;
        }
]);