inject.define("core.app.agro", [
        "corePatternClient.angular.angular",
        "corePatternClient.angular.view",
        "corePatternClient.angular.angular",
        "corePatternClient.angular.controller", 
        "corePatternClient.service.socketio",
        "corePatternClient.service.realtime",
        "corePatternClient.angular.directive",
        "corePatternClient.angular.service",
        "corePatternClient.service.restful",
        "core.util.utils",        
        "core.util.qrcode",
        "core.app.agro-topo",
        "core.app.agro-service",
        "core.app.agro-cadastrar",
        "core.app.agro-home",
        "core.app.agro-login",
        "core.app.agro-listados",
        "core.app.agro-detalhe",
        "core.app.agro-detalhe-tarefa",
        "core.app.agro-detalhe-tarefa-iniciada",
        "core.app.agro-detalhe-tarefa-iniciada-modal",
        "core.app.agro-detalhe-tarefa-iniciada-gravar-audio",
        function(angular) {
                var self = {};
                angular.start();
                return self;
        }
]);