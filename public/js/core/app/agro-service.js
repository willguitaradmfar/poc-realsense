inject.define("core.app.agro-service", [        
        "corePatternClient.angular.service",
        "corePatternClient.angular.directive",
        "core.config.config",        
        "corePatternClient.filter.unsafe",
        function(service, directive, config, filter) {
                var self = {};

                directive.addDirective('temporizador', ['$timeout', function ($timeout) {
                    return {
                        restrict : 'A',
                        scope : {
                            hover : '=hover',
                            max : '=max'
                        },
                        link : function (scope, element, attr) {
                            
                            var reset = function () {
                                $(element).css(
                                    {
                                        'background-image' : 'url(\'img/btn/fnd_hover_azul.jpg\')',
                                        'background-repeat' : 'no-repeat',
                                        'background-position-x' : '-800px'
                                    }
                                );
                            };

                            reset();

                            scope.$watch('hover', function () {
                                console.log(scope.hover);
                                if(scope.hover){
                                    $(element).animate({
                                      'background-position-x': scope.max || '-550px'
                                    }, config.temporizador);
                                }else{
                                    $(element).finish();
                                     $(element).animate({
                                      'background-position-x': '-800px'                                      
                                    }, 500);
                                }
                            });                        
                        }
                    }
                }]);

                service.addService('DB', function($resource){
                        return $resource('/:opr/:key/:value', [], [], {});
                });

                service.addService('User', function($resource){
                        var self = {};
                        self.user = {};
                        return self;                    
                });

                service.addService('OS', function($resource){
                        var self = {};
                        self.os = {};
                        return self;                    
                });

                service.addService('OSDetalheTarefa', function($resource){
                        var self = {};
                        self.detalheTarefa = {};
                        return self;                    
                });


                service.addService('OSCollection', ['$http', function($http){
                        var self = {};

                        self.oss = [];

                        self.reset = function () {
                            $http.get('/rest/os.json').success(function (data) {                                
                                self.oss = data;
                                console.log(data);
                            });
                        };

                        self.reset();                        
                        
                        return self;                    
                }]);



                return self;
        }
]);