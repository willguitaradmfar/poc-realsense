inject.define("core.app.saude-pront-modal", [
        "corePatternClient.angular.view",        
        "core.config.config",        
        function(view, config) {
                var self = {};              
               
                view.addView('prontModal', ['$scope', 'RealTimeService', 'QuadranteService', 'SaudeCollectionService', 'UserService', 
                    function($scope, RealTimeService, QuadranteService, SaudeCollectionService, UserService){

                    $scope.options = [{
                        nome : 'Não aplicar agora',
                        url : '#/pront'
                      },{
                        nome : 'Aplicar medicação',
                        type : 'action'
                      }];
                    
                    if(!SaudeCollectionService.saude){
                        location.href = '#/login';
                        return;
                    }

                    var imparPar = (UserService.user.ID % 2 == 0 ? 'par' : 'impar');

                    $scope.paciente = SaudeCollectionService.saude[imparPar].pacientes['10'];

                    $scope.paciente.dtEntrada = new Date($scope.paciente.dtEntrada || new Date().getTime());                    

                    RealTimeService.removeListen('realsense::faceRecognition');

                    $scope.paciente.horarios.map(function (o, i) {
                        o.horario = moment().add((1*i), 'hours').toDate();
                    });

                    $scope.horario = $scope.paciente.horarios[0];
                    $scope.medicamento = $scope.horario.medicamentos[0];

                   var select;

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0' || data == 'hand=many'){
                                cacheQuadrante = undefined;
                                for(var i in $scope.options){
                                    $scope.options[i]._class = '';
                                    select = undefined;
                                    $scope.$digest();
                                }
                            } if(data == 'hand=click'){
                                click();
                            }
                            console.log(data);
                        });    
                    }, config.timeout);


                   var click = function(){
                        if(!select)return;

                        if(select.url){
                            location.href = select.url;    
                            return;
                        }
                        
                        if(select.type == 'action'){                           

                            SaudeCollectionService.medicamento = $scope.medicamento;
                            SaudeCollectionService.horario = $scope.horario;
                            location.href = '#/aplicarMedicamento';
                            return;
                        }
                        
                        $scope.selected = select;
                        $scope.$digest();
                   };

                   var timeOutClick;
                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                           click();
                        },config.temporizador);
                   };

                    var cacheQuadrante;

                    setTimeout(function () {
                        new QuadranteService.Quadrante({
                            qtdeQuadranteX : 2,
                            qtdeQuadranteY : 1,
                            fns : function(index){

                                if(cacheQuadrante == index)return;
                                cacheQuadrante = index;

                                var y = index.split('_')[1];
                                y = (parseInt(y) * -1) + (1 - 1);

                                var x = index.split('_')[0];
                                x = (parseInt(x) * -1) + (2 - 1);

                                for(var i in $scope.options){
                                    $scope.options[i]._class = undefined;
                                }

                                $scope.options[x]._class = 'hvr-sweep-to-top';
                                select = $scope.options[x];
                                
                                $scope.$digest();
                                changeOption(); 
                            }
                        });
                    }, config.timeout);
                }], ['/dependency/moment/moment.js']);

                return self;
        }
]);