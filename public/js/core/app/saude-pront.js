inject.define("core.app.saude-pront", [
        "corePatternClient.angular.view",        
        "core.config.config",        
        function(view, config) {
                var self = {};              
               
                view.addView('pront', ['$scope', 'RealTimeService', 'QuadranteService', 'SaudeCollectionService', 'UserService', 
                    function($scope, RealTimeService, QuadranteService, SaudeCollectionService, UserService){

                    $scope.options = [{
                        nome : 'Voltar',
                        url : '#/login'
                      },{
                        nome : 'Aplicar Medicamento',
                        url : '#/lista'
                      },{
                        nome : 'Exames Médicos',
                        url : '#/listaexames'
                      },{
                        nome : 'Dados Clínicos',
                        url : '#/pront'
                      },{
                        nome : 'Histórico Médico',
                        url : '#/pront'
                      }];
                    
                    if(!SaudeCollectionService.saude){
                        location.href = $scope.options[0].url;
                        return;
                    }

                    var imparPar = (UserService.user.ID % 2 == 0 ? 'par' : 'impar');

                    $scope.paciente = SaudeCollectionService.saude[imparPar].pacientes['10'];

                    $scope.paciente.dtEntrada = new Date($scope.paciente.dtEntrada || new Date().getTime());                    

                    $scope.paciente.horarios.map(function (o, i) {
                        o.horario = moment().add((2*i), 'hours').toDate();
                    });

                    RealTimeService.removeListen('realsense::faceRecognition');

                   var select;

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0' || data == 'hand=many'){
                                cacheQuadrante = undefined;
                                for(var i in $scope.options){
                                    $scope.options[i]._class = '';
                                    select = undefined;
                                    $scope.$digest();
                                }
                            } if(data == 'hand=click'){
                                click();
                            }
                            console.log(data);
                        });    
                    }, config.timeout);


                   var click = function(){
                        if(select && !select.url)return;

                        location.href = select.url;
                        
                        $scope.selected = select;
                        $scope.$digest();
                   };

                   var timeOutClick;
                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                           click();
                        },config.temporizador);
                   };

                    var cacheQuadrante;

                    setTimeout(function () {
                        new QuadranteService.Quadrante({
                            qtdeQuadranteX : 3,
                            qtdeQuadranteY : 3,
                            fns : function(index){

                                if(cacheQuadrante == index)return;
                                cacheQuadrante = index;

                                var y = index.split('_')[1];
                                y = (parseInt(y) * -1) + (3 - 1);

                                var x = index.split('_')[0];
                                x = (parseInt(x) * -1) + (3 - 1);

                                for(var i in $scope.options){
                                    $scope.options[i]._class = undefined;
                                }

                                if(x == 0 && y == 1){
                                    $scope.options[0]._class = 'hvr-sweep-to-top';
                                    select = $scope.options[0];
                                }else if(x == 0 && y == 2){
                                    $scope.options[0]._class = 'hvr-sweep-to-top';
                                    select = $scope.options[0];
                                }else if(x == 1 && y == 1){
                                    console.log(x, y);
                                    $scope.options[1]._class = 'hvr-sweep-to-top';
                                    select = $scope.options[1];
                                }else if(x == 2 && y == 1){
                                    $scope.options[2]._class = 'hvr-sweep-to-top';
                                    select = $scope.options[2];
                                }else if(x == 1 && y == 0){
                                    $scope.options[3]._class = 'hvr-sweep-to-top';
                                    select = $scope.options[3];
                                }else if(x == 2 && y == 0){
                                    $scope.options[4]._class = 'hvr-sweep-to-top';
                                    select = $scope.options[4];
                                }
                                
                                $scope.$digest();
                                changeOption(); 
                            }
                        });
                    }, config.timeout);
                }], ['/dependency/moment/moment.js']);

                return self;
        }
]);