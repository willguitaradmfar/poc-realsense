inject.define("core.app.agro-detalhe-tarefa", [
        "corePatternClient.angular.view",        
        "core.config.config",        
        function(view, config) {
                var self = {};
                
                view.addView('detalheTarefa', ['$scope', 'RealTimeService', 'QuadranteService', 'DBService', 'SocketIOService', 'UserService', 'OSService', 'OSDetalheTarefaService', function($scope, RealTimeService, QuadranteService, DBService, SocketIOService, UserService, OSService,  OSDetalheTarefaService){
                    
                    var timeOutClick;
                    var select;
                    var cacheQuadrante;

                    $scope.detalheTarefa = OSDetalheTarefaService.detalheTarefa;
                    $scope.os = OSService.os;
                    RealTimeService.removeListen('realsense::faceRecognition');
                    
                    $scope.options = [{
                        nome : 'Voltar',
                        url : '#/detalhe'
                    },{
                        nome : 'Iniciar tarefa',
                        url : '#/detalheTarefaIniciada'
                    }];

                    $scope.user = UserService.user;

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0' || data == 'hand=many'){
                                cacheQuadrante = undefined;
                                for(var i in $scope.options){
                                    $scope.options[i]._class = '';
                                    select = undefined;                                    
                                }
                                
                                $scope.$digest();
                            } if(data == 'hand=click'){
                                click();
                            }
                            console.log(data);
                        });    
                    }, config.timeout);

                   var click = function(){                        
                        console.log(select);

                        if(select && select.url == '#/detalheTarefaIniciada'){
                            $scope.detalheTarefa.iniciado = true;
                            $scope.detalheTarefa.dtini = new Date();
                        }

                        if(select && select.url){
                           location.href = select.url;
                           return;
                        }
                        $scope.selected = select;                        
                        $scope.$digest();
                   };

                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                          click();
                        },config.temporizador);
                   };

                    setTimeout(function () {
                        new QuadranteService.Quadrante({
                            qtdeQuadranteX : 1,
                            qtdeQuadranteY : 2,
                            fns : function(index){
                                
                                if(cacheQuadrante == index)return;
                                cacheQuadrante = index;

                                osIndex = index.split('_')[1];
                                index = index.split('_')[0];
                                
                                index = (parseInt(index) * -1) + ($scope.options.length - 1);
                                
                                for(var i in $scope.options){
                                    $scope.options[i]._class = undefined;
                                }
                                
                                $scope.options[osIndex]._class = 'hvr-sweep-to-top';
                                select = $scope.options[osIndex];                                
                                                                
                                $scope.$digest();
                                changeOption();                            
                            }
                        });
                    }, config.timeout);
                }]);

                return self;
        }
]);