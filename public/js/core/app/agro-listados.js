inject.define("core.app.agro-listados", [
        "corePatternClient.angular.view",        
        "core.config.config",        
        function(view, config) {
                var self = {};
                
                view.addView('listados', ['$scope', 'RealTimeService', 'QuadranteService', 'DBService', 'SocketIOService', 'UserService', 'OSService', 'OSCollectionService', 
                    function($scope, RealTimeService, QuadranteService, DBService, SocketIOService, UserService, OSService, OSCollectionService){
                    
                    var timeOutClick;
                    var select;
                    var cacheQuadrante;

                    RealTimeService.removeListen('realsense::faceRecognition');
                    
                    $scope.options = [{
                        nome : 'Voltar',
                        url : '#/login'
                    }];

                    $scope.oss = OSCollectionService.oss;
                    
                    for(var i = 0 ; i < $scope.oss.length ; i++){
                        var os = $scope.oss[i];
                        os.tarefaConcluida = 0;
                        for(var ii = 0 ; ii < os.detalhe.length ; ii++){
                            var d = os.detalhe[ii];
                            if(d.concluido){
                                os.tarefaConcluida++;                                
                            }
                        }

                        os.tarefaConcluidaP = (os.tarefaConcluida * 100) / os.detalhe.length ;
                    }

                    $scope.user = UserService.user;

                    setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0' || data == 'hand=many'){
                                cacheQuadrante = undefined;
                                for(var i in $scope.options){
                                    $scope.options[i]._class = '';
                                    select = undefined;                                    
                                }
                                for(var i in $scope.oss){
                                    $scope.oss[i]._class = '';
                                }
                                $scope.$digest();
                            } if(data == 'hand=click'){
                                click();
                            }
                            console.log(data);
                        });    
                    }, config.timeout);

                   var click = function(){                        
                        console.log(select);
                        if(select && select.url && select.type != 'os'){
                           location.href = select.url;
                           return;
                        }

                        if(select && select.type == 'os'){
                            OSService.os = select;
                            location.href = select.url;
                            return;
                        }

                        $scope.selected = select;                        
                        $scope.$digest();
                   };

                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                          click();
                        },config.temporizador);
                   };

                    setTimeout(function () {
                        new QuadranteService.Quadrante({
                            qtdeQuadranteX : 2,
                            qtdeQuadranteY : ($scope.oss.length + 1),
                            fns : function(index){
                                
                                 if(cacheQuadrante == index)return;
                                cacheQuadrante = index;
                                osIndex = index.split('_')[1];
                                index = index.split('_')[0];
                                
                                index = (parseInt(index) * -1) + ($scope.options.length - 1);
                                
                                for(var i in $scope.options){
                                    $scope.options[i]._class = undefined;
                                }
                                
                                for(var i in $scope.oss){
                                    $scope.oss[i]._class = undefined;
                                }

                                if(osIndex == 0 && index == 0){
                                    $scope.options[index]._class = 'hvr-sweep-to-top';
                                    select = $scope.options[index];
                                }
                                
                                if(osIndex > 0){
                                    $scope.oss[(osIndex-1)]._class = 'hvr-sweep-to-top';
                                    select = $scope.oss[(osIndex-1)];
                                }
                                
                                $scope.$digest();
                                changeOption();                            
                            }
                        });
                    }, config.timeout);
                }]);

                return self;
        }
]);