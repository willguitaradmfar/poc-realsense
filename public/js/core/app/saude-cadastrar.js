inject.define("core.app.saude-cadastrar", [
        "corePatternClient.angular.view",
        "core.config.config",
        "corePatternClient.angular.directive",
        function(view, config, directive) {
                var self = {};

                view.addView('cadastrar', ['$scope', 'RealTimeService', 'QuadranteService', 'DBService', 'SocketIOService', function($scope, RealTimeService, QuadranteService, DBService, SocketIOService){

                    var cacheFaceRecognition;
                    var taked;
                    var select;
                    var timeOutClick;
                    var cacheQuadrante;

                    $scope.user = {};

                    $scope.ok = function(){

                        DBService.get({opr : 'set', key : 'User'+$scope.ID, value : JSON.stringify($scope.user)});

                        $scope.user = {};

                        DBService.get({opr : 'get', key : 'User'+$scope.ID}, function(data){
                            if(!data.value)return;
                            $scope.alert = 'cadastrado';
                            $scope.user= JSON.parse(data.value);
                        });
                    };                    

                    RealTimeService.setListen('realsense::faceRecognition', function(data){
                        $scope.ID = encodeURI(data).toString().replace(/%00/g, '').replace(/%20/g, ' ');                        

                        if($scope.ID > 0){
                            if(!$scope.user)$scope.user = {};

                            cacheFaceRecognition = $scope.ID;

                            $scope.alert = 'id_encontrado';

                            DBService.get({opr : 'get', key : 'User'+$scope.ID}, function(data){

                                if(!taked && !data.value){

                                    /*var ctx = document.getElementById('qr-canvas').getContext('2d');                                
                                    var dataURL = document.getElementById('qr-canvas').toDataURL("image/jpeg", 0.1);

                                    DBService.save({opr : 'set', key : 'UserPhoto'+$scope.ID}, {value : dataURL}, function(data){
                                       SocketIOService.emit('publish', {
                                                channel : 'realsense::takePhoto',
                                                data : dataURL
                                        });

                                       taked = $scope.ID;
                                    });*/
                                }                                
                            
                                if(!data.value)return;
                                $scope.alert = 'cadastrado';

                                RealTimeService.removeListen('realsense::speechRecognition');

                                $scope.user= JSON.parse(data.value);

                                SocketIOService.emit('publish', {
                                    channel : 'realsense::userautenticado',
                                    data : $scope.user
                                });

                            });                                    
                        }else if($scope.ID < 0){
                            $scope.alert = 'calculando';
                        }
                        $scope.$digest();
                    });                    

                    $scope.options = [{
                        nome : 'Voltar',
                        url : '#/home'
                    },{
                        nome : 'Confirmar'
                    }];

                    setTimeout(function(){
                        RealTimeService.setListen('realsense::speechRecognition', function(data){                           
                            if(!$scope.user) $scope.user = {};

                            $scope.user.nome = data;

                            $scope.$digest();

                        });    
                    }, config.timeout);

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0' || data == 'hand=many'){
                                cacheQuadrante = undefined;
                                for(var i in $scope.options){
                                    $scope.options[i]._class = '';
                                    select = undefined;
                                    $scope.$digest();
                                }
                            } if(data == 'hand=click'){
                                click();

                            }
                            console.log(data);
                        });    
                    }, config.timeout);

                   var click = function(){
                        if(select && select.url){
                           location.href = select.url;
                        }

                        if(select && select.nome == 'Confirmar'){                            
                           $scope.ok();
                        }
                        $scope.selected = select;                        
                        $scope.$digest();
                   };

                    var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                          click();
                        },config.temporizador);
                    };

                    setTimeout(function () {
                        new QuadranteService.Quadrante({
                            qtdeQuadranteX : $scope.options.length,
                            qtdeQuadranteY : 1,
                            fns : function(index){

                                if(cacheQuadrante == index)return;
                                cacheQuadrante = index;

                                index = index.split('_')[0];
                                index = (parseInt(index) * -1) + ($scope.options.length - 1);
                                
                                for(var i in $scope.options){
                                    $scope.options[i]._class = undefined;
                                }                           

                                $scope.options[index]._class = 'hvr-sweep-to-top';
                                select = $scope.options[index];
                                $scope.$digest();
                                changeOption();                            
                            }
                        });
                    }, config.timeout);

                }]);                

                return self;
        }
]);