inject.define("core.app.agro-detalhe-tarefa-iniciada-gravar-audio", [
        "corePatternClient.angular.view",
        "core.config.config",
        "corePatternClient.angular.directive",
        function(view, config, directive) {
                var self = {};

                directive.addDirective('time', ['$timeout', function ($timeout) {
                    return {
                        restrict : 'A',
                        template : '<span data-ng-bind="time"></span>',
                        link : function (scope, element, attr) {
                            var timeout;

                            var ele = $(element);
                            var seconds = 0;
                            var min = 0;
                            var time = function () {
                                seconds++;
                                if(seconds >= 60){
                                    seconds = 0;
                                    min++;
                                }
                                scope.time = (min <= 9 ? '0'+min : min)+':'+(seconds <= 9 ? '0'+seconds : seconds);
                                $timeout(time, 1000);
                            };

                            time();
                        }
                    }
                }]);
                
                view.addView('detalheTarefaIniciadaGravarAudio', ['$scope', 'RealTimeService', 'QuadranteService', 'DBService', 'SocketIOService', 'UserService', 'OSService', 'OSDetalheTarefaService', 
                    function($scope, RealTimeService, QuadranteService, DBService, SocketIOService, UserService, OSService,  OSDetalheTarefaService){
                    
                    var timeOutClick;
                    var select;
                    var cacheQuadrante;

                    $scope.detalheTarefa = OSDetalheTarefaService.detalheTarefa;
                    $scope.os = OSService.osIndex;
                    RealTimeService.removeListen('realsense::faceRecognition');
                    
                    $scope.options = [{
                        nome : 'Cancelar gravação',
                        url : '#/detalheTarefaIniciadaModal'
                    },{
                        nome : 'Salvar Gravação',
                        type : 'action'
                    }];

                    $scope.user = UserService.user;

                    $scope.speechs = [];

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::speechRecognition', function(data){
                            /*if(data.toLowerCase() == 'limpar'){
                                $scope.speechs = [];
                                $scope.$digest();
                                return;
                            }*/
                            $scope.speechs.push(data);
                            $scope.$digest();
                        });    
                    }, config.timeout);

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0' || data == 'hand=many'){
                                cacheQuadrante = undefined;
                                for(var i in $scope.options){
                                    $scope.options[i]._class = '';
                                    select = undefined;                                    
                                }                                
                                $scope.$digest();
                            } if(data == 'hand=click'){
                                click();
                            }                            
                        });    
                    }, config.timeout);

                   var click = function(){                        
                        console.log(select);                      

                        if(select && select.url){
                           location.href = select.url;
                           return;
                        }

                        if(select && select.type == 'action' && select.nome == 'Salvar Gravação'){
                            
                            var text = '';
                            for(var i = 0 ; i < $scope.speechs.length ; i++){
                                text += ($scope.speechs[i] + '<br/>');
                            }

                           $scope.detalheTarefa.descricao = text;

                           
                            location.href =  '#/detalheTarefaIniciada';

                           return;
                        }

                        $scope.selected = select;                        
                        $scope.$digest();
                   };

                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                          click();
                        },config.temporizador);
                   };

                    setTimeout(function () {
                        new QuadranteService.Quadrante({
                            qtdeQuadranteX : $scope.options.length,
                            qtdeQuadranteY : 1,
                            fns : function(index){
                                
                                if(cacheQuadrante == index)return;
                                cacheQuadrante = index;

                                osIndex = index.split('_')[1];
                                index = index.split('_')[0];

                                index = (parseInt(index) * -1) + ($scope.options.length - 1);
                                                               

                                for(var i in $scope.options){
                                    $scope.options[i]._class = undefined;
                                }                                
                                
                                if($scope.options[index].url && osIndex == 1) return;
                                
                                $scope.options[index]._class = 'hvr-sweep-to-top';
                                select = $scope.options[index];
                                                                
                                $scope.$digest();
                                changeOption();                            
                            }
                        });
                    }, config.timeout);
                }]);

                return self;
        }
]);