inject.define("core.app.agro-detalhe-tarefa-iniciada-modal", [
        "corePatternClient.angular.view",        
        "core.config.config",        
        function(view, config) {
                var self = {};
                
                view.addView('detalheTarefaIniciadaModal', ['$scope', 'RealTimeService', 'QuadranteService', 'DBService', 'SocketIOService', 'UserService', 'OSService', 'OSDetalheTarefaService', 
                    function($scope, RealTimeService, QuadranteService, DBService, SocketIOService, UserService, OSService,  OSDetalheTarefaService){
                    
                    var timeOutClick;
                    var select;
                    var cacheQuadrante;

                    $scope.detalheTarefa = OSDetalheTarefaService.detalheTarefa;
                    $scope.os = OSService.os;
                    RealTimeService.removeListen('realsense::faceRecognition');
                    
                    $scope.options = [{
                        nome : 'Não, voltar para lista',
                        url : '#/detalheTarefaIniciada'
                    },{
                        nome : 'GRAVAR ÁUDIO',
                        type : 'action'
                    }];

                    $scope.user = UserService.user;

                   setTimeout(function(){
                        RealTimeService.setListen('realsense::selecionar', function(data){
                            if(data == 'hand=0' || data == 'hand=many'){
                                cacheQuadrante = undefined;
                                for(var i in $scope.options){
                                    $scope.options[i]._class = '';
                                    select = undefined;                                    
                                }                                
                                $scope.$digest();
                            } if(data == 'hand=click'){
                                click();
                            }                            
                        });    
                    }, config.timeout);

                   var click = function(){                        
                        console.log(select);
                        if(select && select.url){
                           location.href = select.url;
                           return;
                        }

                        if(select && select.type == 'action' && select.nome == 'GRAVAR ÁUDIO'){
                           location.href = '#/detalheTarefaIniciadaGravarAudio';
                           return;
                        }                        

                        $scope.selected = select;                        
                        $scope.$digest();
                   };

                   var changeOption = function(){
                        clearTimeout(timeOutClick);
                        timeOutClick = setTimeout(function(){
                          click();
                        },config.temporizador);
                   };

                    setTimeout(function () {
                        new QuadranteService.Quadrante({
                            qtdeQuadranteX : $scope.options.length,
                            qtdeQuadranteY : 1,
                            fns : function(index){
                                
                                if(cacheQuadrante == index)return;
                                cacheQuadrante = index;

                                osIndex = index.split('_')[1];
                                index = index.split('_')[0];

                                index = (parseInt(index) * -1) + ($scope.options.length - 1);
                                                               

                                for(var i in $scope.options){
                                    $scope.options[i]._class = undefined;
                                }                                
                                
                                if($scope.options[index].url && osIndex == 1) return;
                                
                                $scope.options[index]._class = 'hvr-sweep-to-top';
                                select = $scope.options[index];
                                                                
                                $scope.$digest();
                                changeOption();                            
                            }
                        });
                    }, config.timeout);
                }]);

                return self;
        }
]);