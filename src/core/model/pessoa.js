var mongoose = require('mongoose');
var Schema = mongoose.Schema;

inject.define("core.model.pessoa", [   
    "corePattern.mongo.dbAPI",
    function(db) {
		var self = {};
		
		 db.setModel('Historico', {
            lat : 'String',
            lon : 'String',
            date : 'Date',
            dtcad : 'Date',
            bateria : 'Number',
            ip : 'String',
            IMEI : 'String',
            distancia : 'Number',
            velocidade : 'Number'
        });
		
        return self;
    }
]);