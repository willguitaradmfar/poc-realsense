inject.define("core.config.config", [
    function() {
        var self = {};
        self.debug = true;

        self.controlCacheWebsocket = false;

        self.https = true;

        self.maxAge = '0';

        self.pathStatic = 'public/static/';
        self.pathStaticPublic = 'static/';
        self.pathPublic = 'public';
        self.pathFileTmp = '/tmp/fileTmp/';
        self.hostDB = process.env.hostDB || '127.0.0.1';
        self.DB = process.env.DB || 'DBDefault';
        self.PORT = process.env.PORT || 8080;
        self.pathPathUser = 'public/static/user/';
        self.publicPathUser = '/static/user/';
        self.pathPathFeed = 'public/static/feed/';
        self.publicPathFeed = '/static/feed/';
        self.sessionLimit = 1000 * 60 * 60;
        self.sessionSecret = '1234567890QWERTY';
        self.SNSESSION = 'redis';
        self.redisHost = process.env.redisHost || "127.0.0.1";
        self.redisPort = process.env.redisPort || 6379;
        self.timeRefreshCacheKeyRedis = 1000 * 2;
        self.cronRedisQueue = '*/10 * * * * *';

        return self;
    }
]);