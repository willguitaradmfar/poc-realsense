inject = require('injectJS').inject;

console.debug = console.log;
inject.loadLibrary = function(path) {
    console.log('##', 'REQUIRE', path, '##');
    require(path);
};

inject.src = './';
//inject.debug = true;

inject.define("Main", [
	"corePattern.express.app",
    "corePattern.mongo.dbAPI",
	"corePattern.websocket.topic",
	"core.realsense.realsense",
	"core.routes.routes",
    "core.model.pessoa",
    function(app, db) {
        var self = {};        
        app.start();
        //db.start();
        return self;
    }
]);
