//--------------------------------------------------------------------------------------
// Copyright 2015 Intel Corporation
// All Rights Reserved
//
// Permission is granted to use, copy, distribute and prepare derivative works of this
// software for any purpose and without fee, provided, that the above copyright notice
// and this statement appear in all copies.  Intel makes no representations about the
// suitability of this software for any purpose.  THIS SOFTWARE IS PROVIDED 'AS IS.'
// INTEL SPECIFICALLY DISCLAIMS ALL WARRANTIES, EXPRESS OR IMPLIED, AND ALL LIABILITY,
// INCLUDING CONSEQUENTIAL AND OTHER INDIRECT DAMAGES, FOR THE USE OF THIS SOFTWARE,
// INCLUDING LIABILITY FOR INFRINGEMENT OF ANY PROPRIETARY RIGHTS, AND INCLUDING THE
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.  Intel does not
// assume any responsibility for any errors which may appear in this software nor any
// responsibility to update it.
//--------------------------------------------------------------------------------------
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using ServiceStack.Redis;
using ServiceStack.Text;

namespace Face_Recognition
{
    /// <summary>
    /// This sample shows how to recognize faces using RealSense SDK.
    /// </summary>
    class SampleFaceRecognition
    {
        static bool isRegistering = false;

        static string StorageName = "MyDB";

        static RedisClient redisClient = new RedisClient("127.0.0.1");

        static Dictionary<Int32, string> accounts = new Dictionary<Int32, string>();

        static Int32 cacheID;
        static int frame = 0;
        static int frameRelease = 0;        

        static void Main(string[] args)
        {

            // Creating a SDK session
            PXCMSession session = PXCMSession.CreateInstance();

            // Querying the SDK version
            PXCMSession.ImplVersion version = session.QueryVersion();
            Console.WriteLine("RealSense SDK Version {0}.{1}", version.major, version.minor);

            // Creating the SenseManager
            PXCMSenseManager senseManager = session.CreateSenseManager();

            if (senseManager == null)
            {
                Console.WriteLine("Failed to create the SenseManager object.");
                return;
            }

            // Enabling the Face module
            pxcmStatus enablingModuleStatus = senseManager.EnableFace();

            if (enablingModuleStatus != pxcmStatus.PXCM_STATUS_NO_ERROR)
            {
                Console.WriteLine("Failed to enable the Face Module");
                return;
            }

            // Getting the instance of the Face Module
            PXCMFaceModule faceModule = senseManager.QueryFace();
            if (faceModule == null)
            {
                Console.WriteLine("Failed to query the face module");
                return;
            }

            // Creating an active configuration
            PXCMFaceConfiguration faceConfiguration = faceModule.CreateActiveConfiguration();

            if (faceConfiguration == null)
            {
                Console.WriteLine("Failed to create the face configuration");
                return;
            }

            // Track 2D Face
            // faceConfiguration.SetTrackingMode(PXCMFaceConfiguration.TrackingModeType.FACE_MODE_COLOR); 

            // Track 3D Face
            faceConfiguration.SetTrackingMode(PXCMFaceConfiguration.TrackingModeType.FACE_MODE_COLOR_PLUS_DEPTH);

            // Enabling Recognition
            PXCMFaceConfiguration.RecognitionConfiguration rcfg = faceConfiguration.QueryRecognition();
            if (rcfg == null)
            {
                Console.WriteLine("Failed to create the recognition configuration");
                return;
            }
            rcfg.Enable();



            // Set the recognition registration mode
            var desc = new PXCMFaceConfiguration.RecognitionConfiguration.RecognitionStorageDesc();
            desc.maxUsers = 10;
            desc.isPersistent = true;

            var rcfg1 = faceConfiguration.QueryRecognition();
            rcfg1.CreateStorage(StorageName, out desc);
            rcfg1.UseStorage(StorageName);
            rcfg1.SetRegistrationMode(PXCMFaceConfiguration.RecognitionConfiguration.RecognitionRegistrationMode.REGISTRATION_MODE_CONTINUOUS);


            /* if (File.Exists(StorageName))
             {
                 var bytes = File.ReadAllBytes(StorageName);
                 rcfg1.SetDatabaseBuffer(bytes);
                 redisClient.Set(StorageName, bytes);
             }*/

            if (redisClient.Get(StorageName) != null)
            {
                rcfg1.SetDatabaseBuffer(redisClient.Get(StorageName));

                /*IRedisSubscription redisSubscription = null;

                using (redisSubscription = redisClient.CreateSubscription())
                {
                    redisSubscription.OnMessage += (channel, message) =>
                    {
                        Console.WriteLine(message);
                    };
                }

                redisSubscription.SubscribeToChannels(new string[] { "adduser" });*/


            }

            Console.WriteLine("Accurracy {0} - Registration Mode {1}", rcfg.GetAccuracryThreshold(), rcfg.GetRegistrationMode());

            // faceConfiguration.strategy = PXCMFaceConfiguration.TrackingStrategyType.STRATEGY_RIGHT_TO_LEFT;

            // Applying the settings
            if (faceConfiguration.ApplyChanges() < pxcmStatus.PXCM_STATUS_NO_ERROR)
            {
                Console.WriteLine("Failed to apply the configuration");
                return;
            }

            // Initializing the camera
            if (senseManager.Init() < pxcmStatus.PXCM_STATUS_NO_ERROR)
            {
                Console.WriteLine("Failed to initialize the SenseManager");
                return;
            }

            // Creating the Face output           
            PXCMFaceData faceData = faceModule.CreateOutput();
            if (faceData == null)
            {
                Console.WriteLine("Failed to create the facedata object");
                return;
            }
            // Looping to query the faces information
            while (true)
            {
                // Acquiring a frame
                if (senseManager.AcquireFrame(true) < pxcmStatus.PXCM_STATUS_NO_ERROR)
                {
                    break;
                }

                // Updating the face data
                if (faceData != null)
                {
                    faceData.Update();
                }

                // Querying how many faces were detected
                int numberOfFaces = faceData.QueryNumberOfDetectedFaces();
                Console.WriteLine("{0} face(s) were detected.", numberOfFaces);

                if (numberOfFaces == 0)
                {
                    cacheID = 0;
                }

                // Querying the information about detected faces
                /*for (int i = 0; i < numberOfFaces; i++)
                {
                    // Query Face Object
                    PXCMFaceData.Face faceObject = faceData.QueryFaceByIndex(i);
                    if (faceObject == null)
                    {
                        continue;
                    }

                    // Processing Recognition
                    ProcessRecognition(faceData, faceObject);

                }*/

                if (numberOfFaces > 0)
                {
                    PXCMFaceData.Face faceObject = faceData.QueryFaceByIndex(0);
                    if (faceObject == null)
                    {
                        continue;
                    }

                    // Processing Recognition
                    ProcessRecognition(faceData, faceObject);
                }

                // Releasing the acquired frame
                senseManager.ReleaseFrame();
            }




            Console.Write("\nPress any key to continue...");
            Console.ReadKey();

            // Releasing resources
            if (faceData != null) faceData.Dispose();
            if (faceConfiguration != null) faceConfiguration.Dispose();

            faceModule.Dispose();
            senseManager.Close();
            senseManager.Dispose();
            session.Dispose();
        }


        static void ProcessRecognition(PXCMFaceData faceData, PXCMFaceData.Face faceObject)
        {
            // Retrieve the recognition data instance
            PXCMFaceData.RecognitionData recognitionData = faceObject.QueryRecognition();
            frame++;
            frameRelease++;

            Console.WriteLine(">>>>>> frame {0}", frame);
            Console.WriteLine(">>>>>> frameRelease >>>>> {0}", frameRelease);
            // Recognize the current face
            if (recognitionData.IsRegistered())
            {
                frame = 0;
                Int32 userId = recognitionData.QueryUserID();

                string name;

                if (accounts.ContainsKey(userId))
                {
                    name = accounts[userId].ToString();
                }
                else
                {
                    name = "ssss";//System.Text.Encoding.Default.GetString(redisClient.Get("RecognitionID" + userId));
                    accounts.Add(userId, name);
                }

                Console.WriteLine("User Recognized {0}", userId);

                if (cacheID.Equals(userId) && frameRelease < 100)
                {
                    return;
                }
                else
                {
                    cacheID = userId;                    
                }

                string id = userId.ToString();

                byte[] bytes = new byte[id.Length * sizeof(char)];
                System.Buffer.BlockCopy(id.ToCharArray(), 0, bytes, 0, bytes.Length);

                redisClient.Publish("realsense::faceRecognition", bytes);
                frameRelease = 0;

                isRegistering = false;
            }
            else
            {
                frameRelease = 0;
                Console.WriteLine("Not recognized");

                byte[] bytes = new byte["-1".Length * sizeof(char)];
                System.Buffer.BlockCopy("-1".ToCharArray(), 0, bytes, 0, bytes.Length);

                 redisClient.Publish("realsense::faceRecognition", bytes);

                // Read Key pressed to Register User
                if (frame > (100 * 2))
                {
                    frame = 0;

                    var id = recognitionData.RegisterUser();
                    // Retrieve the recognition data instance
                    var rmd = faceData.QueryRecognitionModule();

                    var buffer = new Byte[rmd.QueryDatabaseSize()];

                    rmd.QueryDatabaseBuffer(buffer);

                    redisClient.Set(StorageName, buffer);

                    Console.WriteLine("Cadastrado");
                }
            }
        }
    }
}