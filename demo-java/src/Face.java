import intel.rssdk.PXCMCapture;
import intel.rssdk.PXCMFaceConfiguration;
import intel.rssdk.PXCMFaceData;
import intel.rssdk.PXCMFaceModule;
import intel.rssdk.PXCMRectI32;
import intel.rssdk.PXCMSenseManager;
import intel.rssdk.pxcmStatus;

import java.awt.AWTException;
import java.awt.Robot;

import redis.clients.jedis.Jedis;

public class Face {
    public static void main(String s[]) throws java.io.IOException, AWTException
    {
    	
    	int width = 1366;                    
        int heigth = 768;
    	
    	//Robot robot = new Robot();
    	
        PXCMSenseManager senseMgr = PXCMSenseManager.CreateInstance();
        
        senseMgr.EnableFace(null);        
        
        Jedis jedis = new Jedis("192.168.0.100");
//        jedis.set("foo", "bar");
//        String value = jedis.get("foo");
        
        PXCMFaceModule faceModule = senseMgr.QueryFace();
                
        // Retrieve the input requirements
        pxcmStatus sts = pxcmStatus.PXCM_STATUS_DATA_UNAVAILABLE; 
        PXCMFaceConfiguration faceConfig = faceModule.CreateActiveConfiguration();        
        
        faceConfig.SetTrackingMode(PXCMFaceConfiguration.TrackingModeType.FACE_MODE_COLOR);
        faceConfig.detection.isEnabled = true; 
        faceConfig.landmarks.isEnabled = true; 
        faceConfig.pose.isEnabled = true; 
        faceConfig.ApplyChanges();
        faceConfig.Update();
        
        senseMgr.Init();
                
        PXCMFaceData faceData; // = faceModule.CreateOutput();
        
        for (int nframes=0; nframes<300000; nframes++)
        {
        	senseMgr.AcquireFrame(true);
            
            PXCMCapture.Sample sample = senseMgr.QueryFaceSample();
            
            faceData = faceModule.CreateOutput();
            faceData.Update();
            
            // Read and print data 
            for (int fidx=0; ; fidx++) {
                PXCMFaceData.Face face = faceData.QueryFaceByIndex(fidx);
                if (face==null) break;
                //PXCMFaceData.DetectionData detectData = face.QueryDetection();
                PXCMFaceData.RecognitionData detectData = face.QueryRecognition();                
                
              
                if (detectData != null)
                {
                    PXCMRectI32 rect = new PXCMRectI32();
                    //boolean ret = detectData.QueryBoundingRect(rect);
                    int ret = detectData.QueryUserID();
                    
                    if (ret != -1) {                    	
                        System.out.println("");
                        System.out.println ("Detection Rectangle at frame #" + nframes); 
                        System.out.println ("Top Left corner: (" + rect.x + "," + rect.y + ")" ); 
                        System.out.println ("Height: " + rect.h + " Width: " + rect.w);
                        System.out.println ("ID" + ret);
                        
                    }else{                    	
                    	System.out.println ("Not Registred " + detectData.RegisterUser());                    	
                    }
                } else 
                    break;
                
                PXCMFaceData.PoseData poseData = face.QueryPose();
                if (poseData != null)
                {
                    PXCMFaceData.PoseEulerAngles pea = new PXCMFaceData.PoseEulerAngles();
                    poseData.QueryPoseAngles(pea);
                    // System.out.println ("Pose Data at frame #" + nframes);
                    // System.out.println ("(Roll, Yaw, Pitch) = (" + pea.roll + "," + pea.yaw + "," + pea.pitch + ")");                                        
                    
                    System.out.println(pea.yaw + ", " + pea.pitch);                    
                    
                    jedis.publish("realsense::coordenadas", pea.yaw+","+pea.pitch);
                }  
            }
  
            faceData.close();
            senseMgr.ReleaseFrame();
        }
        
        senseMgr.Close();
        System.exit(0);
    } 
}