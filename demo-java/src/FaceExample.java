
import intel.rssdk.*;

public class FaceExample {

    public static void main(String[] args) {
        // Init
        PXCMSenseManager senseManager = PXCMSession.CreateInstance().CreateSenseManager();
        senseManager.EnableFace(null);
        senseManager.Init();

        PXCMFaceModule faceModule = senseManager.QueryFace();
        PXCMFaceData faceData = faceModule.CreateOutput();

        // Configuration
        PXCMFaceConfiguration moduleConfiguration = faceModule.CreateActiveConfiguration();
        moduleConfiguration.SetTrackingMode(PXCMFaceConfiguration.TrackingModeType.FACE_MODE_COLOR);
        
        PXCMFaceConfiguration.RecognitionConfiguration rcfg=moduleConfiguration.QueryRecognition();
        
	     // Enable face recognition
	     rcfg.Enable();
	      
	     // Create a recognition database
	     PXCMFaceConfiguration.RecognitionConfiguration.RecognitionStorageDesc desc=new PXCMFaceConfiguration.RecognitionConfiguration.RecognitionStorageDesc();
	     desc.maxUsers=10;
	     rcfg.CreateStorage("MyDB", desc);
	     rcfg.UseStorage("MyDB");
	     
	  // Set the registeration mode
	     rcfg.SetRegistrationMode(PXCMFaceConfiguration.RecognitionConfiguration.RecognitionRegistrationMode.REGISTRATION_MODE_CONTINUOUS);

        
        moduleConfiguration.detection.isEnabled = true;
        moduleConfiguration.landmarks.isEnabled = true;
        moduleConfiguration.ApplyChanges();
        moduleConfiguration.close();

        while (true) {
            senseManager.AcquireFrame(true);
            senseManager.QueryFaceSample();
            faceData = faceModule.CreateOutput();
            faceData.Update();

            int numberOfFaces = faceData.QueryNumberOfDetectedFaces();
            if (numberOfFaces > 0) {
                PXCMFaceData.Face face = faceData.QueryFaceByIndex(0);
                
                PXCMFaceData.RecognitionData rdata=face.QueryRecognition();
                
                // recognize the current face?
                int uid=rdata.QueryUserID();
                
                if (uid>=0) {
                
                }
            }
            senseManager.ReleaseFrame();
        }
    }

    /*
        Output:

        Exception in thread "main" java.lang.NoSuchFieldError: confidenceWorld
            at intel.rssdk.PXCMFaceData$LandmarksData.PXCMFaceData_LandmarksData_QueryPoints(Native Method)
            at intel.rssdk.PXCMFaceData$LandmarksData.QueryPoints(PXCMFaceData.java:176)
            at com.parrotsonjava.test.FaceExample.main(FaceExample.java:42)
    */
}